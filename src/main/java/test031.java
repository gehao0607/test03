/**
 * @ClassName test031
 * @Description
 * @Author 87982
 * @Date 2023/4/15 1:07
 * @Version V1.0
 */
public class test031 {
    public static void main(String[] args) {
        System.out.println("test01...第一次写代码");

        System.out.println("test01...第二次写代码，测试git stash");
        System.out.println("test01...第二次写代码，正在写的业务代码，还没写完");
        System.out.println("test01...第三次写代码，业务代码写完了");

        System.out.println("test01...解决线上应用的问题1");
        System.out.println("test01...解决线上应用的问题2");
        System.out.println("test01...解决线上应用的问题3");

        System.out.println("test01...业务代码和线上应用的bug解决...都要");
        System.out.println("准备测试revert1");
        System.out.println("准备测试revert2");
        System.out.println("准备测试revert3");

        System.out.println("提交了错误代码");
        





    }
}
